#
# xnat-test-pg: Dockerfile
# XNAT http://www.xnat.org
# Copyright (c) 2019, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
#
# This is based mostly on the example taken from a post by Robert Malai at 3Pillar Global:
# 
# https://www.3pillarglobal.com/insights/how-to-initialize-a-postgres-docker-million-records
# https://gist.github.com/robert-malai/19d139937a6b51d2709a6584702b4e5e#file-db_setup-sh-L43
#

FROM alpine:3.5

RUN apk update && \
    apk add curl libpq postgresql-client postgresql postgresql-contrib && \
    curl -o /usr/local/bin/gosu -sSL "https://github.com/tianon/gosu/releases/download/1.11/gosu-amd64" && \
    chmod +x /usr/local/bin/gosu && \
    apk del curl && \
    rm -rf /var/cache/apk/*

ENV LANG en_US.utf8
ENV PGDATA /var/lib/postgresql/data/

ENV POSTGRES_DB xnat
ENV POSTGRES_USER xnat
ENV POSTGRES_PASSWORD xnat

RUN mkdir -p /opt/xnat/archive
RUN mkdir -p /opt/xnat/data
RUN mkdir -p /opt/xnat/scripts

COPY ./data/* /opt/xnat/data
COPY ./scripts/* /opt/xnat/scripts/

WORKDIR /opt/xnat/scripts

RUN chmod +x ./*.sh
RUN ./setup-db.sh
RUN ./pack-db.sh

VOLUME $PGDATA

EXPOSE 5432

ENTRYPOINT [ "/opt/xnat/scripts/run-db.sh" ]

CMD [ "postgres" ]


#!/bin/sh
set -e

echo "Packing data folder: ${PGDATA}"

cd /opt/xnat/archive
tar -cf backup.tar -C ${PGDATA} ./
sync
rm -rf ${PGDATA}/*

echo "Pack & clean finished successfully."


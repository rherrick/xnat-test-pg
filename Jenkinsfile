/*
 * xnat-test-pg: Jenkinsfile
 * XNAT http://www.xnat.org
 * Copyright (c) 2019, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

pipeline {
    environment {
        registry = 'xnat/xnat-test-pg'
        registryCredential = 'xnat-docker'
        dockerImage = ''
    }
    agent any
    parameters {
        booleanParam(name: 'FORCE_DEPLOY', defaultValue: false, description: 'Forces the image to be deployed to Docker Hub even if the build is not on the master branch')
    }
    stages {
        stage('Build xnat-test-pg Docker image') {
            steps{
                script {
                    echo "Building xnat-test-pg Docker image from branch ${BRANCH_NAME}: build #${BUILD_NUMBER}"
                    dockerImage = docker.build registry + ":${BUILD_NUMBER}" + (BRANCH_NAME == 'master' ? "" : "-${BRANCH_NAME}")
                }
            }
        }
        stage('Deploy image to Docker Hub on force deploy') {
            when {
                expression { params.FORCE_DEPLOY }
            }
            steps{
                script {
                    echo "Deploying \"${registry}\" image to Docker Hub from branch ${BRANCH_NAME} build #${BUILD_NUMBER} on force deploy (latest tag not updated)"
                    docker.withRegistry('', registryCredential ) {
                        dockerImage.push()
                    }
                }
            }
        }
        stage('Deploy latest image to Docker Hub from master branch') {
            when {
                expression { BRANCH_NAME == 'master' }
            }
            steps{
                script {
                    echo "Deploying \"${registry}\" image to Docker Hub from branch ${BRANCH_NAME} build #${BUILD_NUMBER}, updating latest tag"
                    docker.withRegistry('', registryCredential ) {
                        dockerImage.push()
                        dockerImage.push('latest')
                    }
                }
            }
        }
    }
}

# XNAT Test PostgreSQL #

This is the XNAT test PostgreSQL container image project. This builds a Docker image that provide PostgreSQL 9.6
pre-populated with sample data that can be used as a known reference when composing unit tests for code that can't
run with an H2 database.

## Building ##

### Building locally ###

To build the Docker container image locally:

1. Build the image
1. Tag it
1. Push it to Docker Hub (optional)

For example:

```bash
$ docker build .
$ docker build -t xnat/xnat-test-pg:latest .
$ docker login --username=harmitage --email=harmitage@miskatonic.edu
$ docker push xnat/xnat-test-pg
```

There are [many ways you can build and deploy](https://ropenscilabs.github.io/r-docker-tutorial/04-Dockerhub.html);
this is just a basic procedure for that.

### Building on Jenkins ###

This project includes a [Jenkinsfile](https://jenkins.io/doc/book/pipeline/jenkinsfile/) that defines a pipeline for 
building and deploying the Docker image automatically:

* When code is pushed to any branch in the repository the Docker image is built
* When code is pushed to the **master** branch of the repository the Docker image is deployed to Docker Hub

You can also build manually on the build server. In this case, you'll be prompted to specify a value for the parameter 
**FORCE_DEPLOY**. By default, this parameter is set to **false**. Setting it to **true** causes the build to deploy the
generated image to Docker Hub, even when building from a branch other than **master**.

When a new image is built and pushed to Docker Hub, the image tag is based on the build number:
 
* If the build is on the **master** branch, the image tag is _just_ the build number and is also tagged with **latest**
* If the build is on any branch other than the **master** branch (as in the case of a force deploy), the image tag is 
the build number plus the branch name, e.g. **10-develop** (the **latest** tag is not updated)


